from __future__ import print_function

class Compiler():
    def __init__(self):
        self._output=[]
        self.macros={}
    def get_output(self):
        return '\n'.join(self._output)
    def compile(self,code):
        out=[]
        lines=[]
        for line in code.split("\n"):
            lines.append(line.split(";")[0])
        code=' '.join(lines)

        ops=code.split()

        state="ops"
        for op in ops:
            if state=="string":
                if op.endswith('"'):
                    builtstring.append(op[:-1])
                    builtstring=' '.join(builtstring).decode("string_escape")
                    for char in reversed(builtstring):
                        out.append(">"+("+"*ord(char)))
                    state="ops"
                else:
                    builtstring.append(op)
            elif state=="macro":
                if macroname==None:
                    macroname=op.upper()
                elif op.upper()=="ENDMACRO":
                    macro_text=' '.join(macro_ops)
                    self.macros[macroname]=macro_text
                    macro_ops=[]
                    macroname=None
                    state="ops"
                else:
                    macro_ops.append(op)
            elif state=="import":
                state="ops"
            elif state=="ops":
                oop=op
                op=op.upper()
                try:
                    int(op)
                except: pass
                else:
                    out.append(">"+("+"*int(op)))
                    continue
                
                if op=="MACRO":
                    state="macro"
                    macroname=None
                    macro_ops=[]
                elif op in self.macros:
                    self._output.extend(out)
                    out=[]
                    self.compile(self.macros[op])
                elif op=="IMPORT":
                    state="import"
                elif op[0] in "[],.+-<>":
                    out.append(op)
                elif op.startswith('"'):
                    state="string"
                    builtstring=[oop[1:]]
                    if(op.endswith('"')):
                        state="ops"
                        builtstring=builtstring[0][:-1].decode("string_escape")
                        for char in reversed(builtstring):
                            out.append(">"+("+"*ord(char)))
                else:
                    raise ValueError("unknown op "+op)
        #return '\n'.join(out)
        self._output.extend(out)

#code="""
#;Hello World
#33 100 108 114 111 87 32 44 111 108 108 101 72
#[ OUT ]
#"""

#code="""
#89 110 SWAP OUT
#"""

#code="""
#1 [ POP IN DUP OUT ]
#"""

#code="""
#"Hello, World!" PRINT
#"""

#c=Compiler()
#with open("startup.sbf") as fid:
#    c.compile(fid.read())
#c.compile(code)
#print c.get_output()

if __name__=="__main__":
    import sys
    import re
    import os
    
    if(len(sys.argv)<=1):
        print("Usage: stacker.py [filename]",file=sys.stderr)
        sys.exit(1)
    
    print("collecting files",file=sys.stderr)
    allfiles=[]
    for fname in sys.argv[1:]:
        with open(fname,"r") as fid:
            data=fid.read()
        for match in re.findall(r"IMPORT\s+([0-9a-zA-Z_/\\]+)\s",data,flags=re.IGNORECASE):
            fn=match+".sbf"
            if(os.path.isabs(fn)):
                allfiles.append(fn)
            else:
                path=os.path.dirname(fname)
                allfiles.append(os.path.join(path,fn))
        allfiles.append(fname)
    print("compiling",len(allfiles),"files",file=sys.stderr)
    c=Compiler()
    for fname in allfiles:
        with open(fname,"r") as fid:
            c.compile(fid.read())
    print(c.get_output())




